/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/index";
exports.ids = ["pages/index"];
exports.modules = {

/***/ "./pages/index.tsx":
/*!*************************!*\
  !*** ./pages/index.tsx ***!
  \*************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Home)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var swiper_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! swiper/react */ \"swiper/react\");\n/* harmony import */ var swiper_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! swiper/css */ \"./node_modules/swiper/swiper.min.css\");\n/* harmony import */ var swiper_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(swiper_css__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var swiper_css_effect_coverflow__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! swiper/css/effect-coverflow */ \"./node_modules/swiper/modules/effect-coverflow/effect-coverflow.min.css\");\n/* harmony import */ var swiper_css_effect_coverflow__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(swiper_css_effect_coverflow__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var swiper_css_pagination__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! swiper/css/pagination */ \"./node_modules/swiper/modules/pagination/pagination.min.css\");\n/* harmony import */ var swiper_css_pagination__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(swiper_css_pagination__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var swiper_css_navigation__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! swiper/css/navigation */ \"./node_modules/swiper/modules/navigation/navigation.min.css\");\n/* harmony import */ var swiper_css_navigation__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(swiper_css_navigation__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! swiper */ \"swiper\");\n/* harmony import */ var _static_images_image__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/static/images/image */ \"./static/images/image.ts\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! styled-components */ \"styled-components\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_8__);\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([swiper_react__WEBPACK_IMPORTED_MODULE_1__, swiper__WEBPACK_IMPORTED_MODULE_6__]);\n([swiper_react__WEBPACK_IMPORTED_MODULE_1__, swiper__WEBPACK_IMPORTED_MODULE_6__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);\n\n\n\n\n\n\n\n\n\nfunction Home() {\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        className: \"container\",\n        children: [\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(swiper_react__WEBPACK_IMPORTED_MODULE_1__.Swiper, {\n                effect: \"coverflow\",\n                grabCursor: true,\n                centeredSlides: true,\n                loop: true,\n                slidesPerView: 3,\n                coverflowEffect: {\n                    rotate: 0,\n                    stretch: 0,\n                    depth: 100,\n                    modifier: 2.5\n                },\n                pagination: {\n                    el: \".swiper-pagination\",\n                    clickable: true\n                },\n                navigation: {\n                    nextEl: \".swiper-button-next\",\n                    prevEl: \".swiper-button-prev\"\n                },\n                modules: [\n                    swiper__WEBPACK_IMPORTED_MODULE_6__.EffectCoverflow,\n                    swiper__WEBPACK_IMPORTED_MODULE_6__.Pagination,\n                    swiper__WEBPACK_IMPORTED_MODULE_6__.Navigation\n                ],\n                className: \"swiper_container\",\n                children: _static_images_image__WEBPACK_IMPORTED_MODULE_7__.images.map((image, index)=>{\n                    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(CustomSwiperCard, {\n                        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"img\", {\n                            src: image.imageUrl,\n                            alt: \"slide_image\"\n                        }, void 0, false, {\n                            fileName: \"/Users/joe/Documents/WJAMES-Project/cardswipper/pages/index.tsx\",\n                            lineNumber: 40,\n                            columnNumber: 15\n                        }, this)\n                    }, index, false, {\n                        fileName: \"/Users/joe/Documents/WJAMES-Project/cardswipper/pages/index.tsx\",\n                        lineNumber: 39,\n                        columnNumber: 13\n                    }, this);\n                })\n            }, void 0, false, {\n                fileName: \"/Users/joe/Documents/WJAMES-Project/cardswipper/pages/index.tsx\",\n                lineNumber: 16,\n                columnNumber: 7\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n                className: \"slider-controler\",\n                children: [\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n                        className: \"swiper-button-prev slider-arrow\",\n                        children: \"<\"\n                    }, void 0, false, {\n                        fileName: \"/Users/joe/Documents/WJAMES-Project/cardswipper/pages/index.tsx\",\n                        lineNumber: 46,\n                        columnNumber: 9\n                    }, this),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n                        className: \"swiper-button-next slider-arrow\",\n                        children: \">\"\n                    }, void 0, false, {\n                        fileName: \"/Users/joe/Documents/WJAMES-Project/cardswipper/pages/index.tsx\",\n                        lineNumber: 47,\n                        columnNumber: 9\n                    }, this)\n                ]\n            }, void 0, true, {\n                fileName: \"/Users/joe/Documents/WJAMES-Project/cardswipper/pages/index.tsx\",\n                lineNumber: 45,\n                columnNumber: 7\n            }, this)\n        ]\n    }, void 0, true, {\n        fileName: \"/Users/joe/Documents/WJAMES-Project/cardswipper/pages/index.tsx\",\n        lineNumber: 14,\n        columnNumber: 5\n    }, this);\n}\nconst CustomSwiperCard = styled_components__WEBPACK_IMPORTED_MODULE_8___default()(swiper_react__WEBPACK_IMPORTED_MODULE_1__.SwiperSlide)`\n  background-color: #fff;\n  border-radius: 20px;\n  box-shadow: 0 3px 10px rgb(0 0 0 / 0.2);\n`;\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9pbmRleC50c3guanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBbUQ7QUFDL0I7QUFDaUI7QUFDTjtBQUNBO0FBRWtDO0FBRWxCO0FBQ1I7QUFFeEIsU0FBU08sT0FBTztJQUM3QixxQkFDRSw4REFBQ0M7UUFBSUMsV0FBVTs7MEJBRWIsOERBQUNULGdEQUFNQTtnQkFDTFUsUUFBUTtnQkFDUkMsWUFBWSxJQUFJO2dCQUNoQkMsZ0JBQWdCLElBQUk7Z0JBQ3BCQyxNQUFNLElBQUk7Z0JBQ1ZDLGVBQWU7Z0JBQ2ZDLGlCQUFpQjtvQkFDZkMsUUFBUTtvQkFDUkMsU0FBUztvQkFDVEMsT0FBTztvQkFDUEMsVUFBVTtnQkFDWjtnQkFDQUMsWUFBWTtvQkFBRUMsSUFBSTtvQkFBc0JDLFdBQVcsSUFBSTtnQkFBQztnQkFDeERDLFlBQVk7b0JBQ1ZDLFFBQVE7b0JBQ1JDLFFBQVE7Z0JBRVY7Z0JBQ0FDLFNBQVM7b0JBQUN4QixtREFBZUE7b0JBQUVDLDhDQUFVQTtvQkFBRUMsOENBQVVBO2lCQUFDO2dCQUNsREssV0FBVTswQkFFVEosNERBQVUsQ0FBQyxDQUFDdUIsT0FBT0MsUUFBVTtvQkFDNUIscUJBQ0UsOERBQUNDO2tDQUNDLDRFQUFDQzs0QkFBSUMsS0FBS0osTUFBTUssUUFBUTs0QkFBRUMsS0FBSTs7Ozs7O3VCQURUTDs7Ozs7Z0JBSTNCOzs7Ozs7MEJBRUYsOERBQUNyQjtnQkFBSUMsV0FBVTs7a0NBQ2IsOERBQUNEO3dCQUFJQyxXQUFVO2tDQUFtQzs7Ozs7O2tDQUNsRCw4REFBQ0Q7d0JBQUlDLFdBQVU7a0NBQW1DOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFLMUQsQ0FBQztBQUVELE1BQU1xQixtQkFBbUJ4Qix3REFBTUEsQ0FBQ0wscURBQVdBLENBQUMsQ0FBQzs7OztBQUk3QyxDQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vc3dpcHBlci8uL3BhZ2VzL2luZGV4LnRzeD8wN2ZmIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFN3aXBlciwgU3dpcGVyU2xpZGUgfSBmcm9tIFwic3dpcGVyL3JlYWN0XCI7XG5pbXBvcnQgXCJzd2lwZXIvY3NzXCI7XG5pbXBvcnQgXCJzd2lwZXIvY3NzL2VmZmVjdC1jb3ZlcmZsb3dcIjtcbmltcG9ydCBcInN3aXBlci9jc3MvcGFnaW5hdGlvblwiO1xuaW1wb3J0IFwic3dpcGVyL2Nzcy9uYXZpZ2F0aW9uXCI7XG5cbmltcG9ydCB7IEVmZmVjdENvdmVyZmxvdywgUGFnaW5hdGlvbiwgTmF2aWdhdGlvbiB9IGZyb20gXCJzd2lwZXJcIjtcblxuaW1wb3J0IHsgaW1hZ2VzIH0gZnJvbSBcIkAvc3RhdGljL2ltYWdlcy9pbWFnZVwiO1xuaW1wb3J0IHN0eWxlZCBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gSG9tZSgpIHtcbiAgcmV0dXJuIChcbiAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxuICAgICAgey8qIDxoMSBjbGFzc05hbWU9XCJoZWFkaW5nXCI+Q3VzdG9tIEF2YXRhcjwvaDE+ICovfVxuICAgICAgPFN3aXBlclxuICAgICAgICBlZmZlY3Q9e1wiY292ZXJmbG93XCJ9XG4gICAgICAgIGdyYWJDdXJzb3I9e3RydWV9XG4gICAgICAgIGNlbnRlcmVkU2xpZGVzPXt0cnVlfVxuICAgICAgICBsb29wPXt0cnVlfVxuICAgICAgICBzbGlkZXNQZXJWaWV3PXszfVxuICAgICAgICBjb3ZlcmZsb3dFZmZlY3Q9e3tcbiAgICAgICAgICByb3RhdGU6IDAsXG4gICAgICAgICAgc3RyZXRjaDogMCxcbiAgICAgICAgICBkZXB0aDogMTAwLFxuICAgICAgICAgIG1vZGlmaWVyOiAyLjUsXG4gICAgICAgIH19XG4gICAgICAgIHBhZ2luYXRpb249e3sgZWw6IFwiLnN3aXBlci1wYWdpbmF0aW9uXCIsIGNsaWNrYWJsZTogdHJ1ZSB9fVxuICAgICAgICBuYXZpZ2F0aW9uPXt7XG4gICAgICAgICAgbmV4dEVsOiBcIi5zd2lwZXItYnV0dG9uLW5leHRcIixcbiAgICAgICAgICBwcmV2RWw6IFwiLnN3aXBlci1idXR0b24tcHJldlwiLFxuICAgICAgICAgIC8vIGNsaWNrYWJsZTogdHJ1ZSxcbiAgICAgICAgfX1cbiAgICAgICAgbW9kdWxlcz17W0VmZmVjdENvdmVyZmxvdywgUGFnaW5hdGlvbiwgTmF2aWdhdGlvbl19XG4gICAgICAgIGNsYXNzTmFtZT1cInN3aXBlcl9jb250YWluZXJcIlxuICAgICAgPlxuICAgICAgICB7aW1hZ2VzLm1hcCgoaW1hZ2UsIGluZGV4KSA9PiB7XG4gICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxDdXN0b21Td2lwZXJDYXJkIGtleT17aW5kZXh9PlxuICAgICAgICAgICAgICA8aW1nIHNyYz17aW1hZ2UuaW1hZ2VVcmx9IGFsdD1cInNsaWRlX2ltYWdlXCIgLz5cbiAgICAgICAgICAgIDwvQ3VzdG9tU3dpcGVyQ2FyZD5cbiAgICAgICAgICApO1xuICAgICAgICB9KX1cbiAgICAgIDwvU3dpcGVyPlxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJzbGlkZXItY29udHJvbGVyXCI+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic3dpcGVyLWJ1dHRvbi1wcmV2IHNsaWRlci1hcnJvd1wiPntcIjxcIn08L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJzd2lwZXItYnV0dG9uLW5leHQgc2xpZGVyLWFycm93XCI+e1wiPlwifTwvZGl2PlxuICAgICAgICB7LyogPGRpdiBjbGFzc05hbWU9XCJzd2lwZXItcGFnaW5hdGlvblwiPjwvZGl2PiAqL31cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xufVxuXG5jb25zdCBDdXN0b21Td2lwZXJDYXJkID0gc3R5bGVkKFN3aXBlclNsaWRlKWBcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgYm94LXNoYWRvdzogMCAzcHggMTBweCByZ2IoMCAwIDAgLyAwLjIpO1xuYDtcbiJdLCJuYW1lcyI6WyJTd2lwZXIiLCJTd2lwZXJTbGlkZSIsIkVmZmVjdENvdmVyZmxvdyIsIlBhZ2luYXRpb24iLCJOYXZpZ2F0aW9uIiwiaW1hZ2VzIiwic3R5bGVkIiwiSG9tZSIsImRpdiIsImNsYXNzTmFtZSIsImVmZmVjdCIsImdyYWJDdXJzb3IiLCJjZW50ZXJlZFNsaWRlcyIsImxvb3AiLCJzbGlkZXNQZXJWaWV3IiwiY292ZXJmbG93RWZmZWN0Iiwicm90YXRlIiwic3RyZXRjaCIsImRlcHRoIiwibW9kaWZpZXIiLCJwYWdpbmF0aW9uIiwiZWwiLCJjbGlja2FibGUiLCJuYXZpZ2F0aW9uIiwibmV4dEVsIiwicHJldkVsIiwibW9kdWxlcyIsIm1hcCIsImltYWdlIiwiaW5kZXgiLCJDdXN0b21Td2lwZXJDYXJkIiwiaW1nIiwic3JjIiwiaW1hZ2VVcmwiLCJhbHQiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/index.tsx\n");

/***/ }),

/***/ "./static/images/image.ts":
/*!********************************!*\
  !*** ./static/images/image.ts ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"images\": () => (/* binding */ images)\n/* harmony export */ });\nconst images = [\n    {\n        imageTitle: \"Avatar1\",\n        imageUrl: \"https://cdn.pixabay.com/photo/2020/03/04/04/21/girl-4900483_1280.png\"\n    },\n    {\n        imageTitle: \"Avatar2\",\n        imageUrl: \"https://1.bp.blogspot.com/-SAcIfH7A0aQ/X0nyXHbdacI/AAAAAAAACfg/4iB0Gsj7TEkuquPz2uQibegXuq4mrZgDQCLcBGAsYHQ/s1600/IMG_20200829_095740-min.png\"\n    },\n    {\n        imageTitle: \"Avatar3\",\n        imageUrl: \"https://cdn.pixabay.com/photo/2020/06/29/20/12/akm-gun-5354229_1280.png\"\n    },\n    {\n        imageTitle: \"Avatar1\",\n        imageUrl: \"https://cdn.pixabay.com/photo/2020/03/04/04/21/girl-4900483_1280.png\"\n    },\n    {\n        imageTitle: \"Avatar2\",\n        imageUrl: \"https://1.bp.blogspot.com/-SAcIfH7A0aQ/X0nyXHbdacI/AAAAAAAACfg/4iB0Gsj7TEkuquPz2uQibegXuq4mrZgDQCLcBGAsYHQ/s1600/IMG_20200829_095740-min.png\"\n    },\n    {\n        imageTitle: \"Avatar3\",\n        imageUrl: \"https://cdn.pixabay.com/photo/2020/06/29/20/12/akm-gun-5354229_1280.png\"\n    },\n    {\n        imageTitle: \"Avatar1\",\n        imageUrl: \"https://cdn.pixabay.com/photo/2020/03/04/04/21/girl-4900483_1280.png\"\n    },\n    {\n        imageTitle: \"Avatar2\",\n        imageUrl: \"https://1.bp.blogspot.com/-SAcIfH7A0aQ/X0nyXHbdacI/AAAAAAAACfg/4iB0Gsj7TEkuquPz2uQibegXuq4mrZgDQCLcBGAsYHQ/s1600/IMG_20200829_095740-min.png\"\n    },\n    {\n        imageTitle: \"Avatar3\",\n        imageUrl: \"https://cdn.pixabay.com/photo/2020/06/29/20/12/akm-gun-5354229_1280.png\"\n    }\n];\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zdGF0aWMvaW1hZ2VzL2ltYWdlLnRzLmpzIiwibWFwcGluZ3MiOiI7Ozs7QUFBTyxNQUFNQSxTQUFTO0lBQ3BCO1FBQ0VDLFlBQVk7UUFDWkMsVUFDRTtJQUNKO0lBQ0E7UUFDRUQsWUFBWTtRQUNaQyxVQUNFO0lBQ0o7SUFDQTtRQUNFRCxZQUFZO1FBQ1pDLFVBQ0U7SUFDSjtJQUNBO1FBQ0VELFlBQVk7UUFDWkMsVUFDRTtJQUNKO0lBQ0E7UUFDRUQsWUFBWTtRQUNaQyxVQUNFO0lBQ0o7SUFDQTtRQUNFRCxZQUFZO1FBQ1pDLFVBQ0U7SUFDSjtJQUNBO1FBQ0VELFlBQVk7UUFDWkMsVUFDRTtJQUNKO0lBQ0E7UUFDRUQsWUFBWTtRQUNaQyxVQUNFO0lBQ0o7SUFDQTtRQUNFRCxZQUFZO1FBQ1pDLFVBQ0U7SUFDSjtDQUNELENBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9zd2lwcGVyLy4vc3RhdGljL2ltYWdlcy9pbWFnZS50cz9lOThhIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjb25zdCBpbWFnZXMgPSBbXG4gIHtcbiAgICBpbWFnZVRpdGxlOiBcIkF2YXRhcjFcIixcbiAgICBpbWFnZVVybDpcbiAgICAgIFwiaHR0cHM6Ly9jZG4ucGl4YWJheS5jb20vcGhvdG8vMjAyMC8wMy8wNC8wNC8yMS9naXJsLTQ5MDA0ODNfMTI4MC5wbmdcIixcbiAgfSxcbiAge1xuICAgIGltYWdlVGl0bGU6IFwiQXZhdGFyMlwiLFxuICAgIGltYWdlVXJsOlxuICAgICAgXCJodHRwczovLzEuYnAuYmxvZ3Nwb3QuY29tLy1TQWNJZkg3QTBhUS9YMG55WEhiZGFjSS9BQUFBQUFBQUNmZy80aUIwR3NqN1RFa3VxdVB6MnVRaWJlZ1h1cTRtclpnRFFDTGNCR0FzWUhRL3MxNjAwL0lNR18yMDIwMDgyOV8wOTU3NDAtbWluLnBuZ1wiLFxuICB9LFxuICB7XG4gICAgaW1hZ2VUaXRsZTogXCJBdmF0YXIzXCIsXG4gICAgaW1hZ2VVcmw6XG4gICAgICBcImh0dHBzOi8vY2RuLnBpeGFiYXkuY29tL3Bob3RvLzIwMjAvMDYvMjkvMjAvMTIvYWttLWd1bi01MzU0MjI5XzEyODAucG5nXCIsXG4gIH0sXG4gIHtcbiAgICBpbWFnZVRpdGxlOiBcIkF2YXRhcjFcIixcbiAgICBpbWFnZVVybDpcbiAgICAgIFwiaHR0cHM6Ly9jZG4ucGl4YWJheS5jb20vcGhvdG8vMjAyMC8wMy8wNC8wNC8yMS9naXJsLTQ5MDA0ODNfMTI4MC5wbmdcIixcbiAgfSxcbiAge1xuICAgIGltYWdlVGl0bGU6IFwiQXZhdGFyMlwiLFxuICAgIGltYWdlVXJsOlxuICAgICAgXCJodHRwczovLzEuYnAuYmxvZ3Nwb3QuY29tLy1TQWNJZkg3QTBhUS9YMG55WEhiZGFjSS9BQUFBQUFBQUNmZy80aUIwR3NqN1RFa3VxdVB6MnVRaWJlZ1h1cTRtclpnRFFDTGNCR0FzWUhRL3MxNjAwL0lNR18yMDIwMDgyOV8wOTU3NDAtbWluLnBuZ1wiLFxuICB9LFxuICB7XG4gICAgaW1hZ2VUaXRsZTogXCJBdmF0YXIzXCIsXG4gICAgaW1hZ2VVcmw6XG4gICAgICBcImh0dHBzOi8vY2RuLnBpeGFiYXkuY29tL3Bob3RvLzIwMjAvMDYvMjkvMjAvMTIvYWttLWd1bi01MzU0MjI5XzEyODAucG5nXCIsXG4gIH0sXG4gIHtcbiAgICBpbWFnZVRpdGxlOiBcIkF2YXRhcjFcIixcbiAgICBpbWFnZVVybDpcbiAgICAgIFwiaHR0cHM6Ly9jZG4ucGl4YWJheS5jb20vcGhvdG8vMjAyMC8wMy8wNC8wNC8yMS9naXJsLTQ5MDA0ODNfMTI4MC5wbmdcIixcbiAgfSxcbiAge1xuICAgIGltYWdlVGl0bGU6IFwiQXZhdGFyMlwiLFxuICAgIGltYWdlVXJsOlxuICAgICAgXCJodHRwczovLzEuYnAuYmxvZ3Nwb3QuY29tLy1TQWNJZkg3QTBhUS9YMG55WEhiZGFjSS9BQUFBQUFBQUNmZy80aUIwR3NqN1RFa3VxdVB6MnVRaWJlZ1h1cTRtclpnRFFDTGNCR0FzWUhRL3MxNjAwL0lNR18yMDIwMDgyOV8wOTU3NDAtbWluLnBuZ1wiLFxuICB9LFxuICB7XG4gICAgaW1hZ2VUaXRsZTogXCJBdmF0YXIzXCIsXG4gICAgaW1hZ2VVcmw6XG4gICAgICBcImh0dHBzOi8vY2RuLnBpeGFiYXkuY29tL3Bob3RvLzIwMjAvMDYvMjkvMjAvMTIvYWttLWd1bi01MzU0MjI5XzEyODAucG5nXCIsXG4gIH0sXG5dO1xuIl0sIm5hbWVzIjpbImltYWdlcyIsImltYWdlVGl0bGUiLCJpbWFnZVVybCJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./static/images/image.ts\n");

/***/ }),

/***/ "./node_modules/swiper/modules/effect-coverflow/effect-coverflow.min.css":
/*!*******************************************************************************!*\
  !*** ./node_modules/swiper/modules/effect-coverflow/effect-coverflow.min.css ***!
  \*******************************************************************************/
/***/ (() => {



/***/ }),

/***/ "./node_modules/swiper/modules/navigation/navigation.min.css":
/*!*******************************************************************!*\
  !*** ./node_modules/swiper/modules/navigation/navigation.min.css ***!
  \*******************************************************************/
/***/ (() => {



/***/ }),

/***/ "./node_modules/swiper/modules/pagination/pagination.min.css":
/*!*******************************************************************!*\
  !*** ./node_modules/swiper/modules/pagination/pagination.min.css ***!
  \*******************************************************************/
/***/ (() => {



/***/ }),

/***/ "./node_modules/swiper/swiper.min.css":
/*!********************************************!*\
  !*** ./node_modules/swiper/swiper.min.css ***!
  \********************************************/
/***/ (() => {



/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "styled-components":
/*!************************************!*\
  !*** external "styled-components" ***!
  \************************************/
/***/ ((module) => {

"use strict";
module.exports = require("styled-components");

/***/ }),

/***/ "swiper":
/*!*************************!*\
  !*** external "swiper" ***!
  \*************************/
/***/ ((module) => {

"use strict";
module.exports = import("swiper");;

/***/ }),

/***/ "swiper/react":
/*!*******************************!*\
  !*** external "swiper/react" ***!
  \*******************************/
/***/ ((module) => {

"use strict";
module.exports = import("swiper/react");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/index.tsx"));
module.exports = __webpack_exports__;

})();