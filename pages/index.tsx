import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/pagination";
import "swiper/css/navigation";

import { EffectCoverflow, Pagination, Navigation } from "swiper";

import { images } from "@/static/images/image";
import styled from "styled-components";

export default function Home() {
  return (
    <div className="container">
      {/* <h1 className="heading">Custom Avatar</h1> */}
      <Swiper
        effect={"coverflow"}
        grabCursor={true}
        centeredSlides={true}
        loop={true}
        slidesPerView={3}
        coverflowEffect={{
          rotate: 0,
          stretch: 0,
          depth: 100,
          modifier: 2.5,
        }}
        pagination={{ el: ".swiper-pagination", clickable: true }}
        navigation={{
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
          // clickable: true,
        }}
        modules={[EffectCoverflow, Pagination, Navigation]}
        className="swiper_container"
      >
        {images.map((image, index) => {
          return (
            <CustomSwiperCard key={index}>
              <img src={image.imageUrl} alt="slide_image" />
            </CustomSwiperCard>
          );
        })}
      </Swiper>
      <div className="slider-controler">
        <div className="swiper-button-prev slider-arrow">{"<"}</div>
        <div className="swiper-button-next slider-arrow">{">"}</div>
        {/* <div className="swiper-pagination"></div> */}
      </div>
    </div>
  );
}

const CustomSwiperCard = styled(SwiperSlide)`
  background-color: #fff;
  border-radius: 20px;
  box-shadow: 0 3px 10px rgb(0 0 0 / 0.2);
`;
